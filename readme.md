#Continuous Integration build environment for Bitbucket Pipelines

##Based on openjdk:13-jdk-alpine

* Alpine 3.9
* OpenJDK 13
* Maven 3.5.4
* nodeJs 10.14.2
* yarn 1.15.2
* npm 6.4.1
* lerna 3.13.4
* python3.6
* apache-ant
* aws client
* rancher client v2.2.2
* ssh
* git+subtree